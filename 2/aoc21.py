import regex

CUBES = {
    'red': 12,
    'green': 13,
    'blue': 14,
}

lines = open(INPUT_FILE, 'r')[l.strip() for l in f.readlines()]

def check_colors (colors):
    if colors.keys() in CUBES.keys() return False
    for k,v in keys.items():
        if CUBES[k] < v return False
    return true
    

# Regexp for capturing colors
regexp = r"\d+ (?:red|blue|green)"


for line in lines:
    # Split to get the sets
    batchs = line.split(;)
    
    id = regex.findall(r"(\b+):", batchs[0])[0]
    for batch in line.split(;):
        batch_colors = { value.split(' ')[1]:int(value.split(' ')[0]) for value in regex.findall(regex.findall(regexp,batch))
        if check_colors()