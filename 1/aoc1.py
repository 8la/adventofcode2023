# We will use regexp to match the numbers
import regex as re

INPUT_FILE = "input.txt"
TEXT_NUMBERS = {
    "one":'1',
    "two":'2',
    "three":'3',
    "four":'4',
    "five":'5',
    "six":'6',
    "seven":'7',
    "eight":'8',
    "nine":'9'

}

f = open(INPUT_FILE, 'r')

lines = [l.strip() for l in f.readlines()]
codes = []
for line in lines:
    decoded_line_numbers=[]
    regexp = r"\d|one|two|three|four|five|six|seven|eight|nine"
    mixed_line_numbers = [n for n in re.findall(regexp, line, overlapped=True)]

    first = mixed_line_numbers[0]
    if first in TEXT_NUMBERS.keys():
        decoded_line_numbers.append(TEXT_NUMBERS[first])
    else:
        decoded_line_numbers.append(first)

    last = mixed_line_numbers[-1]
    if last in TEXT_NUMBERS.keys():
        decoded_line_numbers.append(TEXT_NUMBERS[last])
    else:
        decoded_line_numbers.append(last)
    print("Línea: {} First: {} Last: {} Decoded: {}".format(line,first,last,decoded_line_numbers))
    print("Append del num: {}".format(int("".join(decoded_line_numbers))))
    codes.append(int("".join(decoded_line_numbers)))

print("Suma: {}".format(sum(codes)))
